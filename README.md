# THM Scribbles

Write-ups for a few Rooms from THM.

## Rooms

| # | Name | Difficulty | OS |
| --- | --- | --- | --- |
| 1 | [Stealth](/Stealth/README.md) | Medium | Windows |
| 2 | [AVenger](/AVenger/README.md) | Easy | Windows |
