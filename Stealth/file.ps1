$FolderPath = "C:\xampp\htdocs\uploads\"

$FileDictionary = @{}

# Populate the initial state of the dictionary with file names and timestamps
$Files = Get-ChildItem -Path $FolderPath
foreach ($file in $Files) {
    $FileDictionary[$file.Name] = $file.LastWriteTime
}

# Watch for changes in the directory
while ($true) {
    Start-Sleep -Seconds 1
    
    # Check for changes in the directory
    $Files = Get-ChildItem -Path $FolderPath
    foreach ($file in $Files) {
        if ($FileDictionary.ContainsKey($file.Name)) {
            # Compare the current timestamp with the stored timestamp
            if ($file.LastWriteTime -ne $FileDictionary[$file.Name]) {
                Write-Host "File $($file.Name) has been modified."
                # Update the dictionary with the new timestamp
                $FileDictionary[$file.Name] = $file.LastWriteTime
				
				         # Check if the file is executable, a PowerShell script, or a pdf document
            $extension = $file.Extension.ToLower()
            if ($extension -eq ".ps1") {
				$scriptPath = "C:\xampp\htdocs\uploads\$($file.Name)"
					try{
				#Invoke-Expression -Command "powershell.exe -ExecutionPolicy Bypass -File $scriptPath"
				
				Start-Job -ScriptBlock { param($scriptPath) powershell.exe -ExecutionPolicy Bypass -File $scriptPath } -ArgumentList $scriptPath

				}

			catch {
    Write-Host "An exception occurred: $_.Exception.Message"
}

				

                #Write-Host "Opening file: $($file.Name)"
                #Start-Process -FilePath $file.FullName
            }
            }
        } else {
            # New file detected
            Write-Host "File $($file.Name) has been added."
            
            # Add the new file to the dictionary
            $FileDictionary[$file.Name] = $file.LastWriteTime
            
            # Check if the file is executable, a PowerShell script, or a pdf document
            $extension = $file.Extension.ToLower()
            if ($extension -eq ".ps1") {
				$scriptPath = "C:\xampp\htdocs\uploads\$($file.Name)"
				
				try{
				#Invoke-Expression -Command "powershell.exe -ExecutionPolicy Bypass -File $scriptPath"
				Start-Job -ScriptBlock { param($scriptPath) powershell.exe -ExecutionPolicy Bypass -File $scriptPath } -ArgumentList $scriptPath
				}

			catch {
    Write-Host "An exception occurred: $_.Exception.Message"
}






			   #Write-Host "Opening file: $($file.Name)"
                #Start-Process -FilePath $file.FullName
            }
        }
    }
    
    # Check for deleted files
    $deletedFiles = @()
    foreach ($fileName in $FileDictionary.Keys) {
        if (-not (Test-Path -Path (Join-Path $FolderPath $fileName))) {
            Write-Host "File $fileName has been deleted."
            # Add the deleted file to the array for removal
            $deletedFiles += $fileName
        }
    }

    # Remove the deleted files from the dictionary
    foreach ($deletedFile in $deletedFiles) {
        $FileDictionary.Remove($deletedFile)
    }
}
